package main

import (
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"

	"github.com/o1egl/paseto"
)

func createPaseto(message string, sub string, key []byte) (token string, err error) {
	symmetricKey := []byte("BILLION DIAMOND COMMAND SILENCE.") // Must be 32 bytes
	now := time.Now()
	exp := now.Add(24 * 5 * time.Hour)
	nbt := now
	jti := uuid.Must(uuid.NewV4(), err).String()
	if err != nil {
		emitLog(err.Error(), "ERROR")
	}

	jsonToken := paseto.JSONToken{
		Audience:   "https://agent-portal.signicat.guru/",
		Issuer:     "https://agent-portal.signicat.guru/",
		Jti:        jti,
		Subject:    sub,
		IssuedAt:   now,
		Expiration: exp,
		NotBefore:  nbt,
	}
	jsonToken.Set("UberHyperSecretMission", message)
	footer := "Special Tactics and Reconnaissance - authorized by The Citadel Council."

	token, err = paseto.NewV2().Encrypt(symmetricKey, jsonToken, footer)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		fmt.Println()
	}
	return token, err
}
func decryptPasetoMessage(encrypted string, key []byte) (returned_json string, err error) {
	// Decrypt data
	var newJsonToken paseto.JSONToken
	var newFooter string
	err = paseto.NewV2().Decrypt(encrypted, key, &newJsonToken, &newFooter)
	if err != nil {
		emitLog(err.Error(), "ERROR")
	}
	return newJsonToken.Get("UberHyperSecretMission"), err
}
