package main

import (
	"time"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"github.com/go-redis/redis/v8"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func initRedis(ctx context.Context, LOCAL bool) (rdb *redis.Client) {
	if LOCAL {
		rdb = redis.NewClient(&redis.Options{Addr: "localhost:6379"})
	} else {
		rdb = redis.NewClient(&redis.Options{Addr: "10.45.208.3:6379"})
	}
	pong, err := rdb.Ping(ctx).Result()
	if err != nil {
		emitLog("Cannot connect to Redis server! - "+err.Error(), "FATAL")
	}
	emitLog("Recieved "+pong+" from Redis :)", "INFO")
	return rdb
}

func getUserCollection(ctx context.Context, LOCAL bool) (collectionRef *firestore.CollectionRef) {
	conf := &firebase.Config{ProjectID: "technology-dev-auth"}
	if LOCAL {
		sa := option.WithCredentialsFile("./local-firestore-key.json")
		app, err := firebase.NewApp(ctx, conf, sa)
		if err != nil {
			emitLog("getUserCollection "+err.Error(), "FATAL")
		}
		client, err := app.Firestore(ctx)
		if err != nil {
			emitLog("getUserCollection2 "+err.Error(), "FATAL")
		}
		return client.Collection("agent-portal-users")
	} else {
		app, err := firebase.NewApp(ctx, conf)
		if err != nil {
			emitLog("Cannot connect to Firestore! (NewApp) - "+err.Error(), "FATAL")
		}

		client, err := app.Firestore(ctx)
		if err != nil {
			emitLog("Cannot connect to Firestore! (client) - "+err.Error(), "FATAL")
		}
		return client.Collection("agent-portal-users")
	}
}

func getUser(ctx context.Context, collectionRef firestore.CollectionRef, sub string) (user User, err error) {
	dsnap, err := collectionRef.Doc(sub).Get(ctx)
	if err != nil {
		return user, err
	}
	dsnap.DataTo(&user)
	return user, err
}

func isUserSpectre(ctx context.Context, collectionRef firestore.CollectionRef, sub string) (answer bool, err error) {
	dsnap, err := collectionRef.Doc(sub).Get(ctx)
	if err != nil {
		return false, err
	}
	var user User
	dsnap.DataTo(&user)
	if user.Role == "SPECTRE" {
		return true, err
	} else {
		return false, err
	}
}

func createUser_ifNotExist(ctx context.Context, collectionRef firestore.CollectionRef, user User, sub string) (returned_user User, err error) {
	dsnap, err := collectionRef.Doc(sub).Get(ctx)
	if status.Code(err) == codes.NotFound {
		emitLog("User "+sub+" not found, creating!", "INFO")
		_, err = saveUser(ctx, collectionRef, user, sub)
		if err != nil {
			emitLog(err.Error(), "ERROR")
		}
	}
	if err != nil {
		return user, err
	}
	dsnap.DataTo(&user)
	return user, err
}

func saveUser(ctx context.Context, collectionRef firestore.CollectionRef, user User, sub string) (res *firestore.WriteResult, err error) {
	res, err = collectionRef.Doc(sub).Set(ctx, user)
	if err != nil {
		emitLog(err.Error(), "ERROR")
	}
	return res, err
}

func saveSession(ctx context.Context, rdb *redis.Client, session_id, sub string) (err error) {
	err = rdb.Set(ctx, session_id, sub, 1*time.Hour).Err()
	if err != nil {
		emitLog(err.Error(), "ERROR")
	}
	return err
}
