FROM golang:1.16-buster as builder

WORKDIR /app

COPY . .
COPY ./tpl/* ./tpl/
RUN go mod download

RUN go build -v -o server

FROM debian:buster-slim
RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ca-certificates && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY --from=builder /app/tpl/* /app/tpl/
COPY --from=builder /app/server /app/server

CMD ["./server"]