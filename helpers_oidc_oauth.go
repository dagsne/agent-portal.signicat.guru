package main

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

func GetPublicKeys(jwks_uri string) (rsakeys map[string]*rsa.PublicKey, err error) {
	rsakeys = make(map[string]*rsa.PublicKey)
	var body map[string]interface{}
	resp, _ := http.Get(jwks_uri)
	json.NewDecoder(resp.Body).Decode(&body)
	for _, bodykey := range body["keys"].([]interface{}) {
		key := bodykey.(map[string]interface{})
		kid := key["kid"].(string)
		rsakey := new(rsa.PublicKey)
		number, _ := base64.RawURLEncoding.DecodeString(key["n"].(string))
		rsakey.N = new(big.Int).SetBytes(number)
		rsakey.E = 65537
		rsakeys[kid] = rsakey
	}
	return rsakeys, nil
}

func verifyAccessToken(rsakeys map[string]*rsa.PublicKey, tokenString, issuer, aud, scope string) bool {
	emitLog(tokenString, "DEBUG")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return rsakeys[token.Header["kid"].(string)], nil
	})
	if err != nil {
		emitLog(err.Error(), "ERROR")
		return false
	}
	if !token.Valid {
		emitLog(err.Error(), "WARNING")
		return false
	}
	if token.Header["alg"] == nil {
		emitLog("Empty alg not allowed", "WARNING")
		return false
	}
	if !strings.Contains(token.Claims.(jwt.MapClaims)["iss"].(string), issuer) {
		emitLog("Invalid iss", "INFO")
		return false
	}
	if aud != "" {
		if !strings.Contains(token.Claims.(jwt.MapClaims)["aud"].(string), aud) {
			emitLog("Invalid aud", "INFO")
			return false
		}
	}
	if scope != "" {

		if !strings.Contains(fmt.Sprintf("%v", token.Claims.(jwt.MapClaims)["scope"]), scope) {
			emitLog("Specified scope missing", "WARNING")
			return false
		}
	}
	return true

}

func getAccessTokenCC(token_endpoint, client_id, client_secret, scope string) (tokens []byte, err error) {
	payload := strings.NewReader("grant_type=client_credentials&client_id=" + client_id + "&client_secret=" + client_secret + "&scope=" + scope)
	req, _ := http.NewRequest("POST", token_endpoint, payload)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	cred := base64.URLEncoding.EncodeToString([]byte(client_id + ":" + client_secret))
	emitLog(cred, "DEBUG")
	req.Header.Add("Authorization", "Basic "+cred)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		emitLog(err.Error(), "ERROR")
	}
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	emitLog(string(body), "DEBUG")
	return body, err
}

func getBearerToken(bearerToken string) (bearer_token string, err error) {
	emitLog(bearerToken, "DEBUG")
	splitToken := strings.Split(bearerToken, "Bearer")
	if len(splitToken) != 2 {
		emitLog("Weird bearer token (getBearerToken)", "ERROR")
		return "", errors.New("invalid bearer token")
	}
	bearerToken = strings.TrimSpace(splitToken[1])
	emitLog("Provided bearer token: "+bearerToken, "DEBUG")
	return bearerToken, nil
}
