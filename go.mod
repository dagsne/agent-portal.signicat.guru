module signicat.dev/fustep-go-auth

go 1.16

require (
	cloud.google.com/go/firestore v1.5.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/coreos/go-oidc/v3 v3.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis/v8 v8.9.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/o1egl/paseto v1.0.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20210503060351-7fd8e65b6420
	golang.org/x/oauth2 v0.0.0-20210514164344-f6687ab2804c
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
	google.golang.org/api v0.40.0
	google.golang.org/grpc v1.35.0
)
