package main

import (
	"context"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

var (
	LOCAL           = false // ATT: set to true when in local dev env, set to false when deploying!!!
	providerURI_CC  = "https://api.signicat.dev/auth/open"
	providerURI     = "https://team-connect-demo.test.signicat.dev/auth/open"
	clientID        = "dev-substantial-ship-169"
	clientSecret    = "0B0PwMZjWVRxwiw2zyIUhBzzSqZyb0XeTy98dtQqNtLEKdrF"
	clientID_CC     = "preprod-scant-spring-891"
	clientSecret_CC = "rC2jO69O3Bkqb8fzrdBgoLg3PfI8rWB5dRccYeNrvLsqUfhz"
	SCOPE_ADMIN     = "agent-portal:admin"
	SCOPE_READ      = "agent-portal:read"
	aud_CC          = "api://e9dc7fa583f442aab766da8c8ae9f573"
	ctx             = context.Background()
	symmetricKey    = []byte("BILLION DIAMOND COMMAND SILENCE.")
	rdb             = initRedis(ctx, LOCAL)
	user_collection = getUserCollection(ctx, LOCAL)
)
var provider_claims map[string]interface{}
var authConfig oauth2.Config
var authConfig_CC oauth2.Config

func main() {
	rand.Seed(time.Now().Unix())
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	// log.SetLevel(log.DebugLevel) // Uncomment out to set loglevel to DEBUG

	provider, err := oidc.NewProvider(ctx, providerURI)
	if err != nil {
		emitLog("Unable to contact Auth Solution! (NewProvider) - "+err.Error(), "FATAL")
	}
	provider_CC, err := oidc.NewProvider(ctx, providerURI_CC)
	if err := provider.Claims(&provider_claims); err != nil {
		emitLog("Unable to map well-known from Auth Solution into claims! (Claims) - "+err.Error(), "FATAL")
		return
	}
	emitLog(provider_claims["jwks_uri"].(string), "DEBUG")
	authConfig_CC = oauth2.Config{
		ClientID:     clientID_CC,
		ClientSecret: clientSecret_CC,
		Endpoint:     provider_CC.Endpoint(),
		RedirectURL:  "https://agent-portal.signicat.guru/auth/callback",
		Scopes:       []string{"openid", "profile", "offline_access", "nin"},
	}
	authConfig = oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  "https://agent-portal.signicat.guru/auth/callback",
		Scopes:       []string{"openid", "profile", "offline_access", "nin"},
	}
	if LOCAL {
		authConfig.RedirectURL = "https://localhost:8080/auth/callback"
	}

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/auth", authHandler)
	http.HandleFunc("/auth/callback", authcallbackHandler)
	http.HandleFunc("/user", userHandler)
	http.HandleFunc("/logout", logoutHandler)

	http.HandleFunc("/api/token", tokenHandler)
	http.HandleFunc("/api/uber-hyper-secret-mission", UHSMHandler)
	http.HandleFunc("/api/decode-mission", decodemissionHandler)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		emitLog("defaulting to port "+port, "INFO")
	}
	if LOCAL {
		emitLog("listening on https://localhost:8080", "INFO")
		log.Fatal(http.ListenAndServeTLS("localhost:8080", "server.cert", "server.key", nil))
	} else {
		emitLog("Server listening!", "INFO")
		log.Fatal(http.ListenAndServe(":"+port, nil))
	}
}
