package main

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	mrand "math/rand"
)

func randString(nByte int) (string, error) {
	b := make([]byte, nByte)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b), nil
}

// Agent X mission: Fly out to PLACE and meet your contact CONTACT. Your Mission is to ACTION TARGET
func pickPlace() string {
	places := []string{
		"Wynn – Macau, China",
		"The Bellagio – Las Vegas, USA",
		"Casino de Monte Carlo – Monaco",
		"Casino Grand Lisboa – Macau, China",
		"Atlantis Resort – Bahamas",
		"Casino Metropol - Moscow, Russia",
		"Sun City Casino Resort - South Africa",
		"Resorts World Sentosa - Singapore",
	}
	return places[mrand.Intn(len(places))]
}
func pickContact() string {
	contacts := []string{
		"Q",
		"M",
		"Inspector Gadget",
		"Loki",
		"Wonderwoman",
		"Agent 006",
		"Tony Montana",
		"Don Vito Corleone",
		"The Consigliere",
	}
	return contacts[mrand.Intn(len(contacts))]
}
func pickTarget() string {
	targets := []string{
		"Superman",
		"Thor",
		"Sonny Corleone",
		"The Cookie Monster",
		"Postman Pat",
		"Michelangelo (the artist)",
		"Michelangelo (the TMNT)",
		"Sovereign",
		"GLaDOS",
	}
	return targets[mrand.Intn(len(targets))]
}
func pickAction() string {
	actions := []string{
		"tickle to death",
		"give an arrow to the knee of",
		"mind-flood",
		"kidnap",
		"crush in a game of Tic-tac-toe:",
		"bankrupt at the poker table:",
		"hack the mainframe (AS/400) of",
		"steal the lunch money of ",
		"lick the elbow of",
	}
	return actions[mrand.Intn(len(actions))]
}
func pickGender() string {
	targets := []string{
		"F",
		"M",
		"O",
		"A",
		"N/A",
		"B",
	}
	return targets[mrand.Intn(len(targets))]
}
