package main

import (
	"encoding/json"
	"net/http"

	"github.com/go-redis/redis/v8"
)

func tokenHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Invalid method!", http.StatusNotFound)
		return
	}
	scope := r.URL.Query().Get("scope")
	if scope == "" {
		http.Error(w, "Missing requested scope!", http.StatusBadRequest)
		return
	}
	session_cookie, err := r.Cookie("session_id")
	if err != nil {
		http.Error(w, "Missing session cookie!", http.StatusBadRequest)
		return
	}
	sub, err := rdb.Get(ctx, session_cookie.Value).Result()
	emitLog(sub, "DEBUG")
	if err == redis.Nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Invalid session ID!", http.StatusUnauthorized)
		return
	}

	isSpectre, err := isUserSpectre(ctx, *user_collection, sub)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Failed to verify user role: "+err.Error(), http.StatusInternalServerError)
		return
	}
	if !isSpectre {
		emitLog("User is NOT Spectre!", "WARNING")
		http.Error(w, "Unauthorized!", http.StatusUnauthorized)
		return
	}

	var tokens_raw []byte
	if scope == SCOPE_ADMIN || scope == SCOPE_READ {
		tokens_raw, err = getAccessTokenCC(authConfig_CC.Endpoint.TokenURL, clientID_CC, clientSecret_CC, aud_CC+"/"+scope)
		if err != nil {
			emitLog("Unable to get CC access token!", "ERROR")
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	} else {
		emitLog("Invalid scope specified!", "ERROR")
		http.Error(w, "Invalid or missing scope!", http.StatusBadRequest)
		return
	}
	var tokens Tokens
	json.Unmarshal(tokens_raw, &tokens)
	emitLog(tokens.AccessToken, "DEBUG")

	w.Header().Set("Content-Type", "application/json")
	w.Write(tokens_raw)
}

func UHSMHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Invalid method!", http.StatusNotFound)
		return
	}
	session_cookie, err := r.Cookie("session_id")
	if err != nil {
		http.Error(w, "Missing session cookie!", http.StatusBadRequest)
		return
	}
	sub, err := rdb.Get(ctx, session_cookie.Value).Result()
	if err == redis.Nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Invalid session ID!", http.StatusUnauthorized)
		return
	}

	authz := r.Header.Get("Authorization")
	emitLog(authz, "DEBUG")
	bearerToken, err := getBearerToken(authz)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Invalid authorization header!", http.StatusBadRequest)
		return
	}
	rsakeys, err := GetPublicKeys(provider_claims["jwks_uri"].(string))
	if err != nil {
		emitLog("Unable to get public keys Auth Solution! (GetPublicKeys) - "+err.Error(), "FATAL")
		return
	}
	if !verifyAccessToken(rsakeys, bearerToken, provider_claims["issuer"].(string), aud_CC, SCOPE_READ) {
		emitLog("Bearer token NOT VALID!!!", "WARNING")
		http.Error(w, "Unauthorized!", http.StatusUnauthorized)
		return
	} else {
		emitLog("Valid access token!", "INFO")
	}

	isSpectre, err := isUserSpectre(ctx, *user_collection, sub)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Failed to verify user role: "+err.Error(), http.StatusInternalServerError)
		return
	}
	if !isSpectre {
		emitLog("User is NOT Spectre!", "WARNING")
		http.Error(w, "Unauthorized!", http.StatusUnauthorized)
		return
	}

	pasetoToken, err := createPaseto(
		"Fly out to '"+pickPlace()+"' and meet your contact '"+pickContact()+"'. Your mission is to "+pickAction()+" '"+pickTarget()+"'!",
		sub,
		symmetricKey,
	)
	if err != nil {
		http.Error(w, "Failed to create Paseto: "+err.Error(), http.StatusInternalServerError)
		return
	}
	emitLog("Paseto Token: "+pasetoToken, "DEBUG")

	rd := ApiDataUHSM{pasetoToken}
	rd_json, err := json.Marshal(rd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(rd_json)
}

func decodemissionHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Invalid method!", http.StatusNotFound)
		return
	}
	authz := r.Header.Get("Authorization")
	emitLog(authz, "DEBUG")
	bearerToken, err := getBearerToken(authz)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Invalid authorization header!", http.StatusBadRequest)
		return
	}
	rsakeys, err := GetPublicKeys(provider_claims["jwks_uri"].(string))
	if err != nil {
		emitLog("Unable to get public keys Auth Solution! (GetPublicKeys) - "+err.Error(), "FATAL")
		return
	}
	if !verifyAccessToken(rsakeys, bearerToken, provider_claims["issuer"].(string), aud_CC, SCOPE_ADMIN) {
		emitLog("Bearer token NOT VALID!!!", "WARNING")
		http.Error(w, "Unauthorized!", http.StatusUnauthorized)
		return
	} else {
		emitLog("Valid access token!", "INFO")
	}

	var d ApiDataUHSM
	err = json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Error(w, "Invalid body!", http.StatusBadRequest)
		return
	}
	message, err := decryptPasetoMessage(d.UberHyperSecretMission, symmetricKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rd := ApiDataUHSM{message}
	rd_json, err := json.Marshal(rd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(rd_json)
}
