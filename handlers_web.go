package main

import (
	"net/http"
	"text/template"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/go-redis/redis/v8"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/oauth2"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie("session_id")
	if err != nil {
		// no session/access token found
		t, _ := template.ParseFiles("./tpl/main.gohtml")
		t.Execute(w, nil)
	} else {
		http.Redirect(w, r, "/user", http.StatusSeeOther)
	}
}

func authHandler(w http.ResponseWriter, r *http.Request) {
	state, err := randString(16)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	nonce, err := randString(16)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	setCookie(w, r, "state", state)
	setCookie(w, r, "nonce", nonce)

	http.Redirect(w, r, authConfig.AuthCodeURL(state, oidc.Nonce(nonce)), http.StatusFound)
}

func authcallbackHandler(w http.ResponseWriter, r *http.Request) {
	state, err := r.Cookie("state")
	if err != nil {
		http.Error(w, "state not found", http.StatusBadRequest)
		return
	}
	emitLog(state.Value, "DEBUG")
	nonce, err := r.Cookie("nonce")
	if err != nil {
		http.Error(w, "nonce not found", http.StatusBadRequest)
		return
	}
	emitLog(nonce.Value, "DEBUG")
	if r.URL.Query().Get("state") != state.Value {
		http.Error(w, "state did not match", http.StatusBadRequest)
		return
	}

	provider, err := oidc.NewProvider(ctx, providerURI)
	if err != nil {
		emitLog("Unable to contact Auth Solution! (NewProvider) - "+err.Error(), "FATAL")
	}
	verifier := provider.Verifier(&oidc.Config{ClientID: clientID})
	rsakeys, err := GetPublicKeys(provider_claims["jwks_uri"].(string))
	if err != nil {
		emitLog("Unable to get public keys Auth Solution! (GetPublicKeys) - "+err.Error(), "FATAL")
		return
	}
	oauth2Token, err := authConfig.Exchange(ctx, r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
		return
	}
	idToken, err := verifier.Verify(ctx, rawIDToken)
	if err != nil {
		emitLog("ID token NOT VALID!!!", "WARNING")
		deleteCookie(w, r, "session_id")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	if idToken.Nonce != nonce.Value {
		http.Error(w, "nonce did not match", http.StatusBadRequest)
		return
	}
	emitLog("Valid ID token!", "INFO")
	if !verifyAccessToken(rsakeys, oauth2Token.AccessToken, provider_claims["issuer"].(string), "", "") {
		emitLog("Access token NOT VALID!!!", "WARNING")
		deleteCookie(w, r, "session_id")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	} else {
		emitLog("Valid access token!", "INFO")
	}
	userInfo, err := provider.UserInfo(ctx, oauth2.StaticTokenSource(oauth2Token))
	if err != nil {
		http.Error(w, "Failed to get userinfo: "+err.Error(), http.StatusInternalServerError)
		return
	}
	var claims map[string]interface{}
	if err := userInfo.Claims(&claims); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session_id := uuid.Must(uuid.NewV4(), err).String()

	normalized_name := "Mysterious Stranger"
	if val, ok := claims["name"]; ok {
		normalized_name = val.(string)
	} else if val, ok := claims["given_name"]; ok {
		normalized_name = val.(string)
	}
	normalized_birthday := "2154-04-11"
	if val, ok := claims["birthdate"]; ok {
		normalized_birthday = val.(string)
	}
	user := User{
		SubjectID: claims["sub"].(string),
		Name:      normalized_name,
		Birthday:  normalized_birthday,
		Gender:    pickGender(),
		Picture:   "https://www.personality-database.com/profile_images/68.png",
		Role:      "TRAVEL",
		Aliases:   []string{"Lame Man", "Travel Boy"},
	}

	_, err = createUser_ifNotExist(ctx, *user_collection, user, claims["sub"].(string))
	if err != nil {
		http.Error(w, "Failed to save user data: "+err.Error(), http.StatusInternalServerError)
		return
	}

	emitLog("Creating session: "+session_id+"___"+claims["sub"].(string), "INFO")
	saveSession(ctx, rdb, session_id, claims["sub"].(string))
	setCookiePublic(w, r, "session_id", session_id)
	setCookiePublic(w, r, "id_token", rawIDToken)

	http.Redirect(w, r, "/user", http.StatusSeeOther)
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	session_cookie, err := r.Cookie("session_id")
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	sub, err := rdb.Get(ctx, session_cookie.Value).Result()
	if err == redis.Nil {
		emitLog("Session not found in Redis: "+session_cookie.Value, "WARNING")
		deleteCookie(w, r, "session_id")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	user, err := getUser(ctx, *user_collection, sub)
	if err != nil {
		emitLog(err.Error(), "ERROR")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	if user.Role == "SPECTRE" {
		tmpl := template.Must(template.ParseFiles("./tpl/spectre.gohtml"))
		tmpl.Execute(w, user)
		return
	} else {
		tmpl := template.Must(template.ParseFiles("./tpl/travel.gohtml"))
		tmpl.Execute(w, user)
		return
	}
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	deleteCookie(w, r, "session_id")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}
