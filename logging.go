package main

import log "github.com/sirupsen/logrus"

func emitLog(message, level string) {
	switch level {
	case "FATAL":
		log.WithFields(log.Fields{"severity": level}).Fatal(message)
	case "ERROR":
		log.WithFields(log.Fields{"severity": level}).Error(message)
	case "WARNING":
		log.WithFields(log.Fields{"severity": level}).Warn(message)
	case "DEBUG":
		log.WithFields(log.Fields{"severity": level}).Debug(message)
	case "TRACE":
		log.WithFields(log.Fields{"severity": level}).Trace(message)
	default: // info
		log.WithFields(log.Fields{"severity": level}).Info(message)
	}
}
