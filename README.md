# agent-portal.signicat.guru
## An example implementation of Auth Solution in Go!

1. Open https://agent-portal.signicat.guru/
2. Have fun and be amazed!

### Hint: 

Login with Dummy Idp, 3. and set "Subject" to: 1, 2, 3 or 99, 98, 97 for pre-defined users, or anything else for a new user.

(OR try to login with FUSTEP -> IdP Simulator -> eIDSA, then set "BSN" to above)

## Sexy tech info

* Runs on Google Cloud Platform
* Uses:
  * Google Cloud Run
  * GCP Memorystore for Redis
  * GCP Firestore
  * Google Cloud Storage
* The follow Go packages does the heavy lifting:
  * https://github.com/coreos/go-oidc/v3/oidc
  * https://golang.org/x/oauth2
  * https://github.com/dgrijalva/jwt-go

The only real config that is given is:
```
var (
	providerURI     = "https://api.signicat.dev/auth/open"
	clientID        = "preprod-annoyed-bobcat-585"
	clientSecret    = "Ufaf6WckwRX7tuvoseWREwYVZ4mMrToOL5fpEV3TtozRyAmW"
	clientID_CC     = "preprod-solid-station-879"
	clientSecret_CC = "gS0yLkiEkiuK8bMfeLCtFt80ktf1v0f0GzOAoUTGTOWfvh1T"
)
```

**Everything else is automatic!**

### Technical presentation

https://docs.google.com/presentation/d/13NA-D_hAjg-iVt-AYSwfk_mEjqpPSI_pPlWo2cM-aTQ/edit?usp=sharing
