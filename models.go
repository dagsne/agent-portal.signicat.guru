package main

type User struct {
	SubjectID string   `firestore:"sub,omitempty"`
	Name      string   `firestore:"name_normalized,omitempty"`
	Birthday  string   `firestore:"bday_normalized,omitempty"`
	Gender    string   `firestore:"gender,omitempty"`
	Picture   string   `firestore:"picture,omitempty"`
	Role      string   `firestore:"role,omitempty"`
	Aliases   []string `firestore:"aliases,omitempty"`
}

// BSN=1	->	O6lvqYdUJC0sNQOtGWh8NtT-YTUN1UKKM1QH-pk98ho=
// BSN=2	->	w2qo-GV2mFWYEXakvXCv8c6wmYBEzm8GBqfQeWDnbKc=
// BSN=3	->	26s-2rY4JEm6cIyOLDQSta5YFjdY4nmylKypC7NGxK8=
// BSN=99	->	X27c1SUKew8RfOH6eMfo40CobcDRDURFEo5lLQPCsJ8=
// BSN=98	->	lrzBVeuBQwZ9-Xtsd-2geC6ybptW3cU0Sz-YMvHVReM=
// BSN=97	->	-nFWKxga9yeBnVbEQ2A5TDk3zwmMmbkrGD9r561Hgzw=
// dummy=1	->	A6G1VfGUzaPMifry83eqPDJ3NNoR8e0gXDFIHQ7N2DI=
// dummy=2	->	mFmpmfJKCSbKeu6tz-wpijoj0nmOfIlVXLEAcQAPnZU=
// dummy=3	->	G4iVg0-1RQ2COEpPdsmpQ3y8KStafdE8NCUipPUr4S8=
// dummy=99	->	jIZehX0coXJhX-FIitEqwk0MrRLFIZvow55UP1uxBVA=
// dummy=98	->	qpnVDLuFA0z8BMIOzZesXfu8UtZqA44aIpbrGQCSrUE=
// dummy=97	->	p7GmvP2V3Nu9hpgfpIzn5Uw7Ke06vHATTW4GgKBT5IY=

type ApiDataUHSM struct {
	UberHyperSecretMission string `json:"uber_hyper_secret_mission"`
}

type Tokens struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
	TokenType   string `json:"token_type"`
	Scope       string `json:"scope"`
}
